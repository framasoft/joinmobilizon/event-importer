/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["src/views/**/*.pug"],
  theme: {
    extend: {
      colors: {
        "mbz-yellow": {
          DEFAULT: "#FFD599",
          50: "#FFFFFF",
          100: "#FFFFFF",
          200: "#FFFFFF",
          300: "#FFF7EB",
          400: "#FFE6C2",
          500: "#FFD599",
          600: "#FFBE61",
          700: "#FFA729",
          800: "#F08D00",
          900: "#B86C00",
        },
        "mbz-yellow-alt": {
          DEFAULT: "#FAB12D",
          50: "#FEF4E0",
          100: "#FEECCC",
          200: "#FDDDA5",
          300: "#FCCF7D",
          400: "#FBC055",
          500: "#FAB12D",
          600: "#E99806",
          700: "#B37404",
          800: "#7C5103",
          900: "#452D02",
        },
        "mbz-purple": {
          DEFAULT: "#424056",
          50: "#CAC9D7",
          100: "#BEBDCE",
          200: "#A8A6BC",
          300: "#918EAB",
          400: "#7A779A",
          500: "#666385",
          600: "#54516D",
          700: "#424056",
          800: "#292836",
          900: "#111016",
        },
        "mbz-bluegreen": {
          DEFAULT: "#1E7D97",
          50: "#86D2E7",
          100: "#75CCE4",
          200: "#53BFDD",
          300: "#31B2D6",
          400: "#2599B9",
          500: "#1E7D97",
          600: "#155668",
          700: "#0B3039",
          800: "#02090B",
          900: "#000000",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")],
  separator: "_",
};
