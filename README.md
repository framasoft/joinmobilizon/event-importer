# Mobilizon Event Importer (WIP)

This tool allows to crawl and import events from 3rd-party platforms and post them on Mobilizon or call webhooks. Users can set up synchronisations between groups from sources and destinations so that new events are periodically checked and automatically published.

Supported sources:

- Facebook (using the API)
- Meetup
- Eventbrite
- Any platform providing Schema.org JSON-LD metadata
- ICS feeds

Supported destinations:

- Mobilizon
- Webhook
