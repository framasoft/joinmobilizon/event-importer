"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("fb_users", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        fbId: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        name: Sequelize.DataTypes.TEXT,
        email: Sequelize.DataTypes.TEXT,
        longLivedUserToken: Sequelize.DataTypes.TEXT,
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        userId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          allowNull: false,
          references: {
            model: {
              tableName: "users",
            },
            key: "id",
          },
        },
      });

      await queryInterface.addIndex("fb_users", ["fbId", "userId"], {
        unique: true,
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("fb_users");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
