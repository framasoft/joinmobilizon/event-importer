"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("webhooks", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        targetUrl: {
          allowNull: false,
          type: Sequelize.DataTypes.TEXT,
        },
        name: {
          allowNull: false,
          type: Sequelize.DataTypes.TEXT,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        lastSentAt: {
          allowNull: true,
          type: Sequelize.DATE,
        },
        lastSentErrorMessage: {
          allowNull: true,
          type: Sequelize.DataTypes.TEXT,
        },
        events: {
          allowNull: false,
          type: Sequelize.DataTypes.JSON,
        },
        encryptedToken: {
          allowNull: true,
          type: Sequelize.DataTypes.TEXT,
        },
        encryptedTokenIV: {
          allowNull: true,
          type: Sequelize.DataTypes.TEXT,
        },
        userId: {
          type: Sequelize.DataTypes.INTEGER,
          allowNull: false,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "users",
            },
            key: "id",
          },
        },
      });
      await queryInterface.addIndex("webhooks", ["targetUrl", "userId"], {
        unique: true,
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("webhooks");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
