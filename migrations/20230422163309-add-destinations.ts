"use strict";

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable("destinations", {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequelize.INTEGER,
        },
        targetUrl: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        name: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: false,
        },
        imageUrl: {
          type: Sequelize.DataTypes.TEXT,
          allowNull: true,
        },
        type: {
          type: Sequelize.DataTypes.STRING,
          allowNull: false,
        },
        createdAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        updatedAt: {
          allowNull: false,
          type: Sequelize.DATE,
        },
        userId: {
          type: Sequelize.DataTypes.INTEGER,
          onDelete: "CASCADE",
          references: {
            model: {
              tableName: "users",
            },
            key: "id",
          },
        },
      });
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    const transaction = await queryInterface.sequelize.transaction();
    try {
      await queryInterface.dropTable("sync_relations");
      await queryInterface.dropTable("sync_sources");
      await transaction.commit();
    } catch (err) {
      await transaction.rollback();
      throw err;
    }
  },
};
