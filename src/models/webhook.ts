import {
  Table,
  Column,
  Model,
  BelongsTo,
  CreatedAt,
  UpdatedAt,
  ForeignKey,
  DataType,
  Index,
  AllowNull,
} from "sequelize-typescript";
import { User } from "./user.js";

@Table({ tableName: "webhooks" })
export class WebHook extends Model {
  @Index({
    name: "webhooks_target_url_user_id",
    unique: true,
  })
  @AllowNull(false)
  @Column(DataType.STRING)
  declare targetUrl: string;

  @AllowNull(false)
  @Column(DataType.STRING)
  declare name: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @Column(DataType.DATE)
  declare lastSentAt: Date;

  @Column(DataType.STRING)
  declare lastSentErrorMessage: string;

  @Column(DataType.JSON)
  declare events: Record<string, boolean>;

  @Column(DataType.STRING)
  declare encryptedToken: string;

  @Column(DataType.STRING)
  declare encryptedTokenIV: string;

  @BelongsTo(() => User, "userId")
  declare user: ReturnType<() => User>;

  @AllowNull(false)
  @Index("webhooks_target_url_user_id")
  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  declare userId: number;
}
