export enum EventStatus {
  SCHEDULED = "EventScheduled",
  RESCHEDULED = "EventRescheduled",
  POSTPONED = "EventPostponed",
  MOVED_ONLINE = "EventMovedOnline",
  CANCELLED = "EventCancelled",
}

export enum EventAttendanceMode {
  MIXED = "MixedEventAttendanceMode",
  OFFLINE = "OfflineEventAttendanceMode",
  ONLINE = "OnlineEventAttendanceMode",
}

export enum EventJoinOptions {
  FREE = "FREE",
  RESTRICTED = "RESTRICTED",
  INVITE = "INVITE",
  EXTERNAL = "EXTERNAL",
}

export interface IEvent {
  name: string;
  url: string;
  description: string;
  startDate: Date;
  endDate?: Date;
  eventStatus: EventStatus;
  image?: string;
  eventAttendanceMode?: EventAttendanceMode;
  joinOptions?: EventJoinOptions;
  externalParticipationUrl?: string;
  location?: {
    address: {
      streetAddress: string | null;
      addressLocality: string | null;
      addressRegion: string | null;
      addressCountry: string | null;
      geom: string | null;
    } | null;
    name: string;
  };
  organizer: {
    type: "Organization" | "Person";
    name: string;
    url: string;
  };
  tags: string[];
}
