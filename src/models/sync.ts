import {
  Table,
  Column,
  Model,
  HasMany,
  ForeignKey,
  BelongsTo,
  BelongsToMany,
  CreatedAt,
  UpdatedAt,
  PrimaryKey,
  DataType,
  Unique,
  AllowNull,
  Index,
} from "sequelize-typescript";
import { User } from "./user.js";
import { Destination } from "./destination.js";

export enum SOURCE_TYPES {
  MBZ_SOURCE = "MBZ_SOURCE",
  ICS_SOURCE = "ICS_SOURCE",
  MEETUP_SOURCE = "MEETUP_SOURCE",
  EVENTBRITE_SOURCE = "EVENTBRITE_SOURCE",
  FB_SOURCE = "FB_SOURCE",
}

export enum DESTINATION_TYPES {
  MBZ_DESTINATION = "MBZ_DESTINATION",
  WEBHOOK_DESTINATION = "WEBHOOK_DESTINATION",
}

@Table({ tableName: "sync_sources" })
export class SyncSource extends Model {
  @Column(DataType.STRING)
  declare url: string;

  @Column(DataType.STRING)
  declare name: string;

  @Column(DataType.STRING)
  declare image: string;

  @Column(DataType.STRING)
  declare sourceType: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @Column(DataType.DATE)
  declare lastCrawledAt: Date;

  @Column(DataType.STRING)
  declare lastCrawlErrorMessage: string;

  @HasMany(() => SyncRelation, "sourceId")
  declare syncRelations: SyncRelation[];

  @ForeignKey(() => User)
  @Column(DataType.INTEGER)
  declare userId: number;

  @BelongsTo(() => User, {
    onDelete: "CASCADE",
  })
  declare user: ReturnType<() => User>;
}

@Table({ tableName: "sync_relations" })
export class SyncRelation extends Model {
  @BelongsTo(() => SyncSource, {
    onDelete: "CASCADE",
  })
  declare source: SyncSource;

  @AllowNull(false)
  @Index({
    name: "sync_relations_source_id_destination_id",
    unique: true,
  })
  @ForeignKey(() => SyncSource)
  @Column(DataType.INTEGER)
  declare sourceId: number;

  @BelongsTo(() => Destination, {
    onDelete: "CASCADE",
  })
  declare destination: ReturnType<() => Destination>;

  @AllowNull(false)
  @Index("sync_relations_source_id_destination_id")
  @ForeignKey(() => Destination)
  @Column(DataType.INTEGER)
  declare destinationId: number;

  @Column(DataType.STRING)
  declare refreshRate: string;

  @AllowNull(false)
  @Column(DataType.BOOLEAN)
  declare setParticipationToOrigin: boolean;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;

  @Column(DataType.DATE)
  declare lastSyncedAt: Date;

  @Column(DataType.STRING)
  declare lastSyncErrorMessage: string;

  @HasMany(() => SyncRelationAction)
  declare actions: SyncRelationAction[];
}

@Table({ tableName: "sync_relation_action" })
export class SyncRelationAction extends Model {
  @BelongsTo(() => SyncRelation, {
    onDelete: "CASCADE",
  })
  declare syncRelation: SyncRelation;

  @ForeignKey(() => SyncRelation)
  @Column(DataType.INTEGER)
  declare syncRelationId: number;

  @AllowNull(false)
  @Column(DataType.STRING)
  declare contentHash: string;

  @AllowNull(false)
  @Index({
    name: "sync_relation_action_source_url_published_url",
    unique: true,
  })
  @Column(DataType.STRING)
  declare sourceUrl: string;

  @AllowNull(false)
  @Index("sync_relation_action_source_url_published_url")
  @Column(DataType.STRING)
  declare publishedUrl: string;

  @CreatedAt
  declare createdAt: Date;

  @UpdatedAt
  declare updatedAt: Date;
}
