import { Schema } from "./schema.js";
import { jsonLDBlocks, parseHTML } from "../parsers/html.js";
import { ImportResult } from "./importer.js";
import { GraphQLClient, gql } from "graphql-request";
import { IEvent } from "../models/event.js";
import { downloadURL } from "../utils/http.js";
import { IOrganizer } from "../models/organizer.js";
import sanitizeHtml from "sanitize-html";

export class Meetup extends Schema {
  async importEvent(url: string): Promise<ImportResult<IEvent>> {
    console.debug("Importing single event from Meetup", url);
    const { result: event, body } = await super.importEvent(url);
    const dom = parseHTML(body);
    if (event) {
      console.debug("Improving Meetup event description");
      event.description = sanitizeHtml(
        dom("#event-details .break-words").html() ?? event.description,
      );

      const eventIdMatch = url.match(/\/events\/(\d+)/);
      if (eventIdMatch) {
        const eventId = eventIdMatch[1];
        const tags = await this.fetchTags(eventId);
        event.tags = tags.event.topics.edges.map(({ node: { name } }) => name);
      }
    }
    return { result: event, body };
  }

  async importEvents(url: string): Promise<ImportResult<IOrganizer>> {
    console.debug("Importing multiple events from Meetup", url);
    const groupPageBody = await (await downloadURL(url)).text();
    const groupPageDom = parseHTML(groupPageBody);
    const jsonLD = jsonLDBlocks(groupPageDom).flat();
    const organization = jsonLD.find(
      (elem) => elem["@type"] === "Organization",
    );
    if (organization) {
      organization.description = sanitizeHtml(
        groupPageDom("#about-section + div").html() ?? "",
      );
      organization.name = groupPageDom("#group-name-link").text();
      organization.image = groupPageDom("main section img").attr("src");
    }
    if (groupPageDom("#about-section").toArray().length === 0) {
      throw "URL is not a group page";
    }
    const body = await (await downloadURL(`${url}/events/`)).text();
    const dom = parseHTML(body);
    const urls: string[] = [];
    dom('a[data-event-label^="event-card"]').each((_i, tag) => {
      const link = dom(tag).attr("href");
      if (link) {
        const url = new URL(link, "https://www.meetup.com");
        urls.push(url.toString());
      }
    });
    console.debug("Found following meetup event URLs: ", urls);
    const result = (await Promise.all(
      [...new Set(urls)]
        .map(async (url) => (await this.importEvent(url)).result)
        .filter(async (url) => (await url) !== null),
    )) as IEvent[];
    return Promise.resolve({
      result: {
        events: result,
        name: organization?.name,
        url,
        image: organization?.image,
        description: organization?.description,
      },
      body,
    });
  }

  private async fetchTags(eventId: string) {
    console.debug("fetching tags for eventid", eventId);
    return await graphQLQuery<{
      event: {
        topics: {
          edges: {
            node: {
              id: string;
              name: string;
            };
          }[];
        };
      };
    }>(
      "https://www.meetup.com/gql",
      gql`
        query getEventTopics($eventId: ID!) {
          event(id: $eventId) {
            topics {
              edges {
                node {
                  id
                  name
                  __typename
                }
                __typename
              }
              __typename
            }
            __typename
          }
        }
      `,
      {
        eventId,
      },
    );
  }
}

export async function graphQLQuery<T>(
  endpoint: string,
  query: string,
  variables: Record<string, any>,
) {
  const client = new GraphQLClient(endpoint, {
    headers: {
      "Accept-Language": "fr-FR",
    },
  });

  return await client.request<T>(query, variables);
}
