import { IEvent } from "../models/event.js";

export type ImportResult<T> = { result: T | null; body: string };

export interface Importer {
  importEvent(url: string): Promise<ImportResult<IEvent>>;
}
