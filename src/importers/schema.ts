import { downloadURL } from "../utils/http.js";
import { ImportResult, Importer } from "./importer.js";
import { jsonLDBlocks, parseHTML } from "../parsers/html.js";
import { EventJoinOptions, IEvent } from "../models/event.js";
import sanitizeHtml from "sanitize-html";
import { microdata } from "@cucumber/microdata";
import { JSDOM } from "jsdom";

export class Schema implements Importer {
  async importEvent(url: string): Promise<ImportResult<IEvent>> {
    const body = await (await downloadURL(url)).text();
    let event = this.convertJSONLDElements(jsonLDBlocks(parseHTML(body)), url);
    if (!event) {
      const dom = new JSDOM(body);
      const microDataEvent = microdata(
        "https://schema.org/Event",
        dom.window.document.documentElement,
      );
      event = this.convertEvent(microDataEvent, url);
    }
    console.debug("Imported event", url);
    return { result: event, body };
  }

  protected convertJSONLDElements(data: Record<string, any>[], url: string) {
    const element = data.find((elem) => this.isEvent(elem["@type"])) as Event;
    if (!element) return null;
    return this.convertEvent(element, url);
  }

  protected convertEvent(element: any, url: string): IEvent | null {
    console.debug("Converting event", element);
    if (!element) return null;
    return {
      ...element,
      name: element["name"].trim(),
      eventStatus: (element["eventStatus"] ?? "").slice(19),
      eventAttendanceMode: (element["eventAttendanceMode"] ?? "").slice(19),
      joinOptions: EventJoinOptions.EXTERNAL,
      externalParticipationUrl: url,
      startDate: new Date(element.startDate),
      endDate: element.endDate ? new Date(element.endDate) : undefined,
      tags: (element.keywords ?? []).filter(
        (keyword: any) =>
          typeof keyword === "string" || keyword instanceof String,
      ),
      image: this.absoluteUrl(
        Array.isArray(element.image) && element.image.length > 0
          ? element.image[0]
          : element.image,
        url,
      ),
      description: sanitizeHtml(element["description"]),
      url,
      location: {
        ...element.location,
        address: {
          ...this.handleAddress(element.location.address),
          geom: element.location.geo
            ? `${element.location.geo.longitude};${element.location.geo.latitude}`
            : null,
        },
      },
    } as IEvent;
  }

  protected isEvent(type: string): boolean {
    return [
      "Event",
      "BusinessEvent",
      "ChildrensEvent",
      "ComedyEvent",
      "CourseInstance",
      "DanceEvent",
      "DeliveryEvent",
      "EducationEvent",
      "EventSeries",
      "ExhibitionEvent",
      "Festival",
      "FoodEvent",
      "Hackathon",
      "LiteraryEvent",
      "MusicEvent",
      "PublicationEvent",
      "SaleEvent",
      "ScreeningEvent",
      "SocialEvent",
      "SportsEvent",
      "TheaterEvent",
      "VisualArtsEvent",
    ].includes(type);
  }

  private absoluteUrl(url: string, baseURL: string) {
    return new URL(url, baseURL).href;
  }

  private handleAddress(
    address:
      | string
      | {
          addressCountry: string | { name: string };
          addressLocality: string;
          addressRegion: string;
          postalCode: string;
          streetAddress: string;
        },
  ) {
    if (typeof address === "string" || address instanceof String) {
      return { streetAddress: address };
    }
    return address;
  }
}
