import { Router } from "express";
import { body, oneOf, validationResult } from "express-validator";
import { SOURCE_TYPES, SyncSource } from "../models/sync.js";
import { checkLogin } from "../middlewares/auth.js";
import { FacebookUser } from "../models/index.js";
import { fetchUserPages, getUserProfile } from "../services/facebook.js";

const SourceController = Router();

SourceController.use(checkLogin());

SourceController.get("/", async (req, res) => {
  if (!req.session.user?.id) {
    return res.status(401).render("errors/401");
  }
  const sources = await SyncSource.findAll({
    where: {
      userId: req.session.user.id,
    },
  });
  return res.render("sources/index", {
    sources,
  });
});

SourceController.get("/new", async (req, res) => {
  if (!req.session.user?.id) {
    return res.status(401).render("errors/401");
  }
  const user = req.session.user;
  const facebookUserPages = [];
  const facebookEnabled = Boolean(process.env.FB_ENABLED);
  if (facebookEnabled) {
    const facebookUsers = await FacebookUser.findAll({
      where: {
        userId: user.id,
      },
    });

    for (let fbUser of facebookUsers) {
      const profile = await getUserProfile(fbUser.longLivedUserToken);
      const pages = await fetchUserPages(fbUser.longLivedUserToken);
      facebookUserPages.push({ profile, pages });
    }
  }

  return res.render("sources/new", {
    facebookUserPages,
    facebookEnabled,
  });
});

SourceController.post(
  "/new",
  body("url").isURL(),
  body("name").isString().optional(),
  oneOf([body("image").isURL().optional(), body("image").isEmpty()]),
  body("sourceType")
    .isString()
    .custom((input: string) => {
      return input in SOURCE_TYPES;
    }),
  async (req, res) => {
    const errors = validationResult(req);
    const user = req.session.user;
    if (!user) return res.status(401);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    let imageUrl = req.body.image;
    if (["", "null", "undefined", null].includes(imageUrl)) {
      imageUrl = null;
    }

    const syncSource = new SyncSource({
      url: req.body.url,
      name: req.body.name,
      sourceType: req.body.sourceType,
      image: imageUrl,
      userId: user.id,
    });
    await syncSource.save();
    res.flash("success", res.__("Source correctly saved"));
    return req.session.save(() => res.redirect("/sources"));
  },
);

SourceController.post(
  "/delete",
  oneOf([body("sources").isString().notEmpty(), body("sources").isArray()]),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        console.debug(error);
        res.flash(
          "error",
          res.__("You need to select at least one source to delete"),
        );
      });
      return req.session.save(() => res.redirect("/sources"));
    }
    let sourcesIds = req.body.sources;
    if (!Array.isArray(sourcesIds)) {
      sourcesIds = [sourcesIds];
    }
    const sessionUserId = req.session.user?.id;
    if (!sessionUserId) {
      return res.status(401);
    }

    for (let sourceId of sourcesIds) {
      const syncSource = await SyncSource.findOne({
        where: {
          id: sourceId,
          userId: sessionUserId,
        },
      });
      if (!syncSource) {
        return res.status(403);
      }
      syncSource.destroy();
    }

    res.flash(
      "success",
      res.__n("%s sources successfully deleted", sourcesIds.length),
    );

    return req.session.save(() => res.redirect("/sources"));
  },
);

export { SourceController };
