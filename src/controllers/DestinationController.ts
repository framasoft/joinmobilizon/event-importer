import { Router } from "express";
import { body, oneOf, validationResult } from "express-validator";
import { DESTINATION_TYPES } from "../models/sync.js";
import {
  MobilizonOAuthApplication,
  MobilizonOAuthApplicationToken,
} from "../models/mobilizon_oauth_application.model.js";
import { userMemberships, userProfiles } from "../services/mobilizon.js";
import {
  IMember,
  deduplicateMemberships,
} from "../services/mobilizon/interfaces/member.js";
import validator from "validator";
import { sequelize } from "../db.js";
import { Destination } from "../models/destination.js";
import { checkLogin } from "../middlewares/auth.js";
import { Paginate } from "../services/mobilizon/interfaces/paginate.js";
import { IActor } from "../services/mobilizon/interfaces/actor.js";
import { WebHook } from "../models/webhook.js";

const DestinationController = Router();

DestinationController.use(checkLogin());

DestinationController.get("/", async (req, res) => {
  if (!req.session.user?.id) {
    return res.status(401).render("errors/401");
  }
  const destinations = await Destination.findAll({
    where: {
      userId: req.session.user.id,
    },
  });
  return res.render("destinations/index", {
    destinations,
  });
});

DestinationController.get("/new", async (req, res) => {
  const user = req.session.user;
  if (!user) {
    return res.status(401).render("errors/401");
  }
  console.debug("DestinationController new", user);
  const mobilizonOAuthAppToken = await MobilizonOAuthApplicationToken.findOne({
    where: {
      userId: user.id,
    },
    order: [["createdAt", "DESC"]],
    limit: 1,
    group: ["federatedIdentity", "MobilizonOAuthApplicationToken.id", "app.id"],
    include: MobilizonOAuthApplication,
  });
  if (!mobilizonOAuthAppToken) {
    console.error("No Mobilizon oAuthApplicationToken found for this user");
    return res.redirect("/destinations");
  }
  let memberships: Paginate<IMember> | undefined = { elements: [], total: 0 };
  let profiles: IActor[] = [];
  let deduplicatedMemberships: Record<string, IMember> = {};
  let hasError = false;
  try {
    memberships = await userMemberships(
      mobilizonOAuthAppToken.app.hostname,
      mobilizonOAuthAppToken,
    );
    if (!memberships) {
      return res.redirect("/destinations");
    }
    deduplicatedMemberships = deduplicateMemberships(memberships);

    const profileResult = await userProfiles(
      mobilizonOAuthAppToken.app.hostname,
      mobilizonOAuthAppToken,
    );
    console.debug("found user profiles", profileResult);
    if (profileResult.actors) {
      profiles = profileResult.actors;
    }
  } catch (e) {
    res.flash(
      "error",
      res.__(
        "Error fetching your profiles and groups for Mobilizon account {{user}}",
        { user: user.email },
      ),
    );
    hasError = true;
  }

  const webhooks = await WebHook.findAll({
    where: {
      userId: user.id,
    },
  });

  return res.render("destinations/new", {
    mobilizon: {
      total: memberships?.total,
      hostname: mobilizonOAuthAppToken.app.hostname,
      memberships: Object.values(deduplicatedMemberships),
      profiles: profiles,
      hasError,
    },
    webhooks,
  });
});

DestinationController.post(
  "/new",
  body("destinations").custom((destinationsStrings: string | string[]) => {
    if (!Array.isArray(destinationsStrings)) {
      destinationsStrings = [destinationsStrings];
    }
    return destinationsStrings.every((destinationstring) => {
      const destinationsParams = new URLSearchParams(destinationstring);
      console.debug("destinationsParams", destinationsParams);
      const destinationType = destinationsParams.get("destinationType");
      const url = destinationsParams.get("url");
      const name = destinationsParams.get("name");
      return (
        destinationType != null &&
        destinationType in DESTINATION_TYPES &&
        url != null &&
        // validator.default.isURL(url, { require_host: false }) &&
        name != null
      );
    });
  }),
  async (req, res) => {
    const errors = validationResult(req);
    const user = req.session.user;
    if (!user) return res.status(401);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        console.debug(error);
        if (error.type === "field" && error.path === "destinations") {
          res.flash(
            "error",
            res.__("You need to select at least one destination to add"),
          );
        }
      });
      return req.session.save(() => res.redirect("/destinations/new"));
    }

    let destinationsStrings = req.body.destinations;
    if (!Array.isArray(destinationsStrings)) {
      destinationsStrings = [destinationsStrings];
    }
    const destinationsData = convertDestinations(destinationsStrings);

    try {
      const result = await sequelize.transaction(async (transaction) => {
        return Promise.all(
          destinationsData.map(async (destinationData) => {
            console.debug("Saving destination", destinationData, user.id);
            const syncDestination = new Destination({
              ...destinationData,
              userId: user.id,
            });
            await syncDestination.save({ transaction });
          }),
        );
      });
      console.debug("result", result);
    } catch (error) {
      console.error(error);
    }
    return res.redirect("/destinations");
  },
);

DestinationController.post(
  "/delete",
  oneOf([
    body("destinations").isString().notEmpty(),
    body("destinations").isArray(),
  ]),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        console.debug(error);
        res.flash(
          "error",
          res.__("You need to select at least one destination to delete"),
        );
      });
      return req.session.save(() => res.redirect("/destinations"));
    }
    let destinationsIds = req.body.destinations;
    if (!Array.isArray(destinationsIds)) {
      destinationsIds = [destinationsIds];
    }
    const sessionUserId = req.session.user?.id;
    if (!sessionUserId) {
      return res.status(401);
    }

    for (let destinationId of destinationsIds) {
      const destination = await Destination.findOne({
        where: {
          id: destinationId,
          userId: sessionUserId,
        },
      });
      if (!destination) {
        return res.status(403);
      }

      if (destination.type === DESTINATION_TYPES.WEBHOOK_DESTINATION) {
        // Delete the webhook as well
        const webhook = await WebHook.findOne({
          where: {
            targetUrl: destination.targetUrl,
            userId: sessionUserId,
          },
        });
        await webhook?.destroy();
      }

      destination.destroy();
    }
    return req.session.save(() => res.redirect("/destinations"));
  },
);

const convertDestinations = (
  destinations: string[],
): {
  type: DESTINATION_TYPES;
  targetUrl: string;
  name: string;
  imageUrl: string | null;
}[] => {
  return destinations
    .map((destinationstring) => {
      const destinationsParams = new URLSearchParams(destinationstring);
      const destinationType = destinationsParams.get(
        "destinationType",
      ) as DESTINATION_TYPES;
      const url = destinationsParams.get("url") as string;
      const name = destinationsParams.get("name") as string;
      let imageUrl = destinationsParams.get("imageUrl") ?? null;
      if (["null", "undefined", null].includes(imageUrl)) {
        imageUrl = null;
      }
      return { type: destinationType, targetUrl: url, name, imageUrl };
    })
    .filter(({ type, targetUrl, name }) => type && targetUrl && name);
};

export { DestinationController };
