import { Router } from "express";
import { checkLogin } from "../middlewares/auth.js";
import { Destination, WebHook } from "../models/index.js";
import { body, oneOf, validationResult } from "express-validator";
import { DESTINATION_TYPES } from "../models/sync.js";
import { encrypt } from "../services/encryption.js";
import { config } from "../config.js";

const WebhookController = Router();

WebhookController.use(checkLogin());

WebhookController.get("/new", (req, res) => {
  if (!req.session.user?.id) {
    return res.status(401).render("errors/401");
  }
  return res.render("webhook/new");
});

WebhookController.post(
  "/new",
  body("targetUrl").isURL(),
  body("secretToken").isString(),
  oneOf([
    body("events").isArray(),
    body("events").isString(),
    body("events").optional(),
  ]),
  body("name").isString().notEmpty(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        // @ts-ignore
        console.debug(error, error.nestedErrors);
        if (error.type === "field" && error.path === "targetUrl") {
          res.flash(
            "error",
            res.__("The targetUrl parameter does not seem to be an URL"),
          );
        }
        if (error.type === "field" && error.path === "name") {
          res.flash(
            "error",
            res.__("The name parameter does not seem to be a valid number"),
          );
        }
      });
      return res.status(400).render("webhook/new");
    }

    const targetUrl = req.body.targetUrl;
    const name = req.body.name;
    const secretToken = req.body.secretToken;
    let events = req.body.events ?? (["create"] as string[] | string);

    if (events && !Array.isArray(events)) {
      events = [events];
    }

    let encryptedText;
    let encryptedTextIV;
    if (secretToken) {
      const result = encrypt(secretToken, config.ENCRYPTION_KEY);
      if (!result) {
        res.flash("error", res.__("Failed to encrypt secret token"));
        return res.status(500).render("webhook/new");
      }
      encryptedTextIV = result.iv;
      encryptedText = result.encryptedText;
    }

    if (!req.session.user?.id) {
      return res.status(401).render("errors/401");
    }

    console.debug("Going to save new webhook with", {
      targetUrl,
      name,
      userId: req.session.user.id,
      encryptedToken: encryptedText,
      encryptedTokenIV: encryptedTextIV,
      events,
    });

    let webhook = new WebHook({
      targetUrl,
      name,
      userId: req.session.user.id,
      encryptedToken: encryptedText,
      encryptedTokenIV: encryptedTextIV,
      events,
    });
    webhook = await webhook.save();

    const destination = new Destination({
      targetUrl,
      name,
      type: DESTINATION_TYPES.WEBHOOK_DESTINATION,
      userId: req.session.user.id,
    });

    await destination.save();

    res.flash(
      "success",
      res.__("Webhook successfully saved as new destination"),
    );

    return res.redirect("/destinations");
  },
);

export { WebhookController };
