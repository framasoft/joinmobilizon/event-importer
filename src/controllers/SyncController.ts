import { Router } from "express";
import { body, oneOf, validationResult } from "express-validator";
import { SyncRelation, SyncSource } from "../models/sync.js";
import { Destination } from "../models/destination.js";
import { UniqueConstraintError } from "sequelize";
import { checkLogin } from "../middlewares/auth.js";
import { crawlURL } from "../services/crawl.js";
import { crawlResultIsCrawlOrganizerType } from "../models/crawl_result.js";
import { EventJoinOptions } from "../models/event.js";
import { publishEvent } from "../services/publication.js";
import { performSync } from "../services/syncher.js";

const SyncController = Router();

SyncController.use(checkLogin());

SyncController.get("/new", async (req, res) => {
  const user = req.session.user;
  if (!user) {
    return res.status(401).render("errors/401");
  }
  const sources = await SyncSource.findAll({
    where: {
      userId: user.id,
    },
  });
  const destinations = await Destination.findAll({
    where: {
      userId: user.id,
    },
  });

  return res.render("sync/new", {
    sources,
    destinations,
  });
});

SyncController.post(
  "/new",
  body("sourceId").isNumeric(),
  body("destinationId").isNumeric(),
  body("refreshRate")
    .notEmpty()
    .custom((input: string) => {
      return (
        input.match(
          /^P(([0-9]+Y)?([0-9]+M)?([0-9]+W)?([0-9]+D)?(T([0-9]+H)?([0-9]+M)?([0-9]+(\.?[0-9]+)?S)?)?)?$/,
        ) !== null
      );
    }),
  body("setParticipationToOrigin").isBoolean().optional(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.flash("error", res.__("Error while submitting form"));
      return req.session.save(() => res.redirect("/syncs"));
    }
    if (!req.session.user?.id) {
      return res.status(401).render("errors/401");
    }
    const source = await SyncSource.findOne({
      where: {
        id: req.body.sourceId,
        userId: req.session.user.id,
      },
    });
    const destination = await Destination.findOne({
      where: {
        id: req.body.destinationId,
        userId: req.session.user.id,
      },
    });
    if (!source || !destination) {
      res.flash(
        "error",
        res.__("Error while finding given source or destination"),
      );
      return req.session.save(() => res.redirect("/syncs"));
    }

    console.debug(
      "setParticipationToOrigin val",
      req.body.setParticipationToOrigin ?? false,
    );

    try {
      const syncRelation = new SyncRelation({
        sourceId: req.body.sourceId,
        destinationId: req.body.destinationId,
        refreshRate: req.body.refreshRate,
        setParticipationToOrigin: req.body.setParticipationToOrigin ?? false,
      });
      const savedSyncRelation = await syncRelation.save();
      // Perform initial sync
      const syncRelationWithRelations = await SyncRelation.findByPk(
        savedSyncRelation.id,
        {
          include: [Destination, SyncSource],
        },
      );
      if (!syncRelationWithRelations) {
        res.flash(
          "error",
          res.__("Error fetching sync relation we just saved"),
        );
        return res.status(500);
      }
      await performSync(syncRelationWithRelations);
      res.flash(
        "success",
        res.__(
          'Synchronisation successfully saved and initial synchronisation completed. Currently published upcoming events from "{{source}}" have been republished on "{{destination}}"',
          {
            source: source.name || source.url,
            destination: destination.name || destination.targetUrl,
          },
        ),
      );
    } catch (e) {
      if (e instanceof UniqueConstraintError) {
        console.error("Unique constraint when creating a relation");
      } else {
        throw e;
      }
    }
    return req.session.save(() => res.redirect("/"));
  },
);

SyncController.post(
  "/delete",
  oneOf([body("syncs").isString().notEmpty(), body("syncs").isArray()]),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      res.flash(
        "error",
        res.__("You need to select at least one sync to delete"),
      );
      return req.session.save(() => res.redirect("/"));
    }
    let syncIds = req.body.syncs;
    if (!Array.isArray(syncIds)) {
      syncIds = [syncIds];
    }
    const sessionUserId = req.session.user?.id;
    if (!sessionUserId) {
      return res.status(401).render("errors/401");
    }

    for (let syncId of syncIds) {
      const sync = await SyncRelation.findOne({
        include: [
          {
            model: Destination,
            as: "destination",
            required: true,
          },
          {
            model: SyncSource,
            as: "source",
            required: true,
          },
        ],
        where: {
          id: syncId,
          "$destination.userId$": sessionUserId,
          "$source.userId$": sessionUserId,
        },
      });
      if (!sync) {
        res.flash(
          "error",
          res.__("Failed to delete synchronisation {{syncId}}", { syncId }),
        );
        return req.session.save(() => res.redirect("/"));
      }
      await sync.destroy();
    }
    res.flash(
      "success",
      res.__n("Synchronisations successfully deleted", syncIds.length),
    );
    return req.session.save(() => res.redirect("/"));
  },
);

SyncController.post(
  "/import",
  body("sourceURL").isURL(),
  body("destinationId").isNumeric(),
  body("setParticipationToOrigin").isBoolean().optional(),
  async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      errors.array().forEach((error) => {
        if (error.type === "field" && error.path === "sourceURL") {
          res.flash(
            "error",
            res.__("The sourceURL parameter does not seem to be an URL"),
          );
        }
        if (error.type === "field" && error.path === "destinationId") {
          res.flash(
            "error",
            res.__(
              "The destinationId parameter does not seem to be a valid number",
            ),
          );
        }
      });
      return req.session.save(() => res.redirect("/crawl"));
    }
    if (!req.session.user?.id) {
      return res.status(401).render("errors/401");
    }
    const destination = await Destination.findOne({
      where: {
        id: req.body.destinationId,
        userId: req.session.user.id,
      },
    });
    if (!destination) {
      res.flash("error", res.__("Unable to find the given destination"));
      return req.session.save(() => res.redirect("/crawl"));
    }

    const url = req.body.sourceURL;

    console.debug(
      "setParticipationToOrigin val",
      req.body.setParticipationToOrigin ?? false,
    );

    const result = await crawlURL(url);
    if (crawlResultIsCrawlOrganizerType(result)) {
      res.flash("error", res.__("URL should not link to an organizer page"));
      return req.session.save(() => res.redirect("/crawl"));
    }
    const eventToPublish = result.event;
    if (!eventToPublish) {
      res.flash("error", res.__("Event not found from URL"));
      return req.session.save(() => res.redirect("/crawl"));
    }

    // If we don't want to set the origin to remote, reset external participation options to default
    if (!req.body.setParticipationToOrigin) {
      eventToPublish.externalParticipationUrl = undefined;
      eventToPublish.joinOptions = EventJoinOptions.FREE;
    }

    const publishedEvent = await publishEvent(
      eventToPublish,
      destination,
      Number(req.session.user.id),
    );

    if (!publishedEvent) {
      res.flash("error", res.__("Unable to publish event"));
      return req.session.save(() => res.redirect("/crawl"));
    }

    return req.session.save(() => res.redirect("/"));
  },
);

export { SyncController };
