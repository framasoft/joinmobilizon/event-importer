import { Router } from "express";
import {
  fetchUserPages,
  getUserProfile,
  pageInformation,
} from "../services/facebook.js";
import { downloadURL } from "../utils/http.js";
import { endpointUrl } from "../router.js";
import { FacebookUser } from "../models/index.js";
import { checkLogin } from "../middlewares/auth.js";

const FacebookController = Router();

FacebookController.use(checkLogin());

const appId = process.env.FB_APP_ID;
const appSecret = process.env.FB_APP_SECRET;
const configId = process.env.FB_CONFIG_ID;

FacebookController.get("/login", async (req, res) => {
  return res.redirect(
    302,
    `https://www.facebook.com/v17.0/dialog/oauth?client_id=${appId}&redirect_uri=${endpointUrl(
      req,
    )}/facebook/login-callback&state=toto&config_id=${configId}`,
  );
});

FacebookController.get("/login-callback", async (req, res) => {
  if (!req.session.user?.id) {
    return res.status(401).render("errors/401");
  }
  const code = req.query.code;
  // const state = req.query.state;
  console.debug("Got access code", code);
  const accessTokens = (await (
    await downloadURL(
      `https://graph.facebook.com/v17.0/oauth/access_token?client_id=${appId}&redirect_uri=${endpointUrl(
        req,
      )}/facebook/login-callback&client_secret=${appSecret}&code=${code}`,
    )
  ).json()) as {
    access_token: string;
    token_type: "bearer";
    expires_in: number;
  };
  console.debug("Got short-lived access token", accessTokens.access_token);
  const longTokens = (await (
    await downloadURL(
      `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${appId}&client_secret=${appSecret}&fb_exchange_token=${accessTokens.access_token}`,
    )
  ).json()) as {
    access_token: string;
    token_type: "bearer";
    expires_in: number;
  };
  console.debug("Got long-lived access token", longTokens.access_token);
  const userAccessToken = longTokens.access_token;
  const profile = await getUserProfile(userAccessToken);
  console.debug("Got profile", profile);
  FacebookUser.upsert(
    {
      fbId: profile.id,
      name: profile.name,
      email: profile.email,
      longLivedUserToken: longTokens.access_token,
      userId: req.session.user.id,
    },
    {
      conflictFields: ["fbId", "userId"],
    },
  );
  req.session.fbUser = {
    ...profile,
    access_token: userAccessToken,
  };
  console.debug("saved session", req.session);

  return res.redirect("/");
});

FacebookController.get("/page", async (req, res) => {
  if (!req.query.pageId || !req.session.fbUser?.access_token)
    return res.redirect("/");
  const page = await pageInformation(
    req.query.pageId as string,
    req.session.fbUser?.access_token,
  );
  console.debug("Got page information", page);
  return res.render("facebook/page", {
    page,
  });
});

export { FacebookController };
