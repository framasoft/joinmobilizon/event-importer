import { httpProtocol } from "../router.js";
import { downloadURL } from "../utils/http.js";

export async function finger(federatedIdentity: string) {
  const domain = federatedIdentity.split("@")[1];
  const endpoint = `${httpProtocol()}://${domain}/.well-known/webfinger?resource=acct:${federatedIdentity}`;
  const res = await downloadURL(endpoint);
  return await res.json();
}
