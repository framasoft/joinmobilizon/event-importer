import { Op } from "sequelize";
import {
  CrawlOrganizerResult,
  crawlResultIsCrawlOrganizerType,
} from "../models/crawl_result.js";
import { Destination } from "../models/destination.js";
import { EventJoinOptions, IEvent } from "../models/event.js";
import { SyncRelation, SyncRelationAction } from "../models/sync.js";
import { crawlURL } from "./crawl.js";
import { publishEvent, updatePublishedEvent } from "./publication.js";
import { createHash } from "node:crypto";

export const performSync = async (sync: SyncRelation) => {
  try {
    await performEventSync(sync);
    SyncRelation.update(
      {
        lastSyncedAt: new Date(),
        lastSyncErrorMessage: null,
      },
      {
        where: {
          [Op.and]: {
            sourceId: sync.sourceId,
            destinationId: sync.destinationId,
          },
        },
      },
    );
  } catch (e) {
    console.error(e);
    SyncRelation.update(
      {
        lastSyncedAt: new Date(),
        // @ts-ignore
        lastSyncErrorMessage: e.toString(),
      },
      {
        where: {
          [Op.and]: {
            sourceId: sync.sourceId,
            destinationId: sync.destinationId,
          },
        },
      },
    );
  }
};

export const performEventSync = async (sync: SyncRelation) => {
  const url = sync.source.url;
  const result = await crawlURL(url);
  if (crawlResultIsCrawlOrganizerType(result)) {
    return await performOrganizerSync(
      result,
      sync.source.userId,
      sync.destinationId,
      sync.id,
    );
  }
  const eventToPublish = result.event;
  if (!eventToPublish) {
    throw "Could not fetch event content";
  }
  const destination = await getDestination(sync.destinationId);

  // If we don't want to set the origin to remote, reset external participation options to default
  if (!sync.setParticipationToOrigin) {
    eventToPublish.externalParticipationUrl = undefined;
    eventToPublish.joinOptions = EventJoinOptions.FREE;
  }

  return await doPublishEvent(
    eventToPublish,
    destination,
    sync.source.userId,
    sync.id,
  );
};

export const performOrganizerSync = async (
  result: CrawlOrganizerResult,
  user_id: number,
  destinationId: number,
  syncId: number,
) => {
  const destination = await getDestination(destinationId);
  const eventsToPublish = result.organizer.events;
  console.info(
    `Going to publish ${eventsToPublish.length} events from organizer ${result.organizer.name} to destination ${destination.name}`,
  );
  return Promise.all(
    eventsToPublish.map(async (eventResult) => {
      return await doPublishEvent(eventResult, destination, user_id, syncId);
    }),
  );
};

const getDestination = async (destinationId: number) => {
  const destination = await Destination.findByPk(destinationId);
  if (!destination) {
    throw "Could not find destination with this ID";
  }
  return destination;
};

const doPublishEvent = async (
  sourceEvent: IEvent,
  destination: Destination,
  userId: number,
  syncId: number,
) => {
  const previewRelationAction = await SyncRelationAction.findOne({
    where: {
      syncRelationId: syncId,
      sourceUrl: sourceEvent.url,
    },
  });

  const hash = createHash("sha256");
  hash.update(JSON.stringify(sourceEvent));
  const digest = hash.digest("hex");

  if (previewRelationAction) {
    if (previewRelationAction.contentHash === digest) {
      // Event did not change, skip publishing
      console.debug("Event did not change, skip publishing");
      return;
    } else {
      // Looks like event changed, we need to update it
      console.debug(
        "Event did change since last sync, let's update it publishing",
      );
      const updatedEvent = await updatePublishedEvent(
        sourceEvent,
        previewRelationAction.publishedUrl,
        destination,
        userId,
      );
      if (updatedEvent) {
        previewRelationAction.contentHash = digest;
        await previewRelationAction.save();
      }
      return;
    }
  }
  // New event that we need to publish
  console.debug("New event to publish");
  const publishedEvent = await publishEvent(sourceEvent, destination, userId);
  console.debug(
    "Published event, saving into SyncRelationAction",
    publishedEvent,
  );
  if (!publishedEvent) {
    throw "Event not published";
  }
  await SyncRelationAction.create({
    sourceUrl: sourceEvent.url,
    publishedUrl: publishedEvent.url,
    contentHash: digest,
    syncRelationId: syncId,
  });
};
