// import { downloadURL } from "../utils/http.js";

import { downloadURL } from "../utils/http.js";

export interface FacebookUserSession {
  id: string;
  name: string;
  email: string;
  access_token: string;
}

export async function fetchUserPages(accessToken: string) {
  const result = (await (
    await downloadURL(
      `https://graph.facebook.com/v17.0/me?access_token=${accessToken}&debug=all&fields=id%2Cname%2Caccounts%7Bid%2Cname%2Cabout%2Clink%2Cpicture%7Burl%7D%7D&format=json`,
    )
  ).json()) as {
    id: string;
    name: string;
    accounts: { data: { id: string; name: string }[] };
  };
  // @ts-ignore
  console.debug("FB result", result.accounts.data);
  return result.accounts.data;
}

export async function eventsForPage(pageId: string, userAccessToken: string) {
  const pageAccessToken = await getPageToken(pageId, userAccessToken);
  return await (
    await downloadURL(
      `https://graph.facebook.com/me/events?access_token=${pageAccessToken["access_token"]}`,
    )
  ).json();
}

export async function pageInformation(pageId: string, userAccessToken: string) {
  const pageAccessToken = await getPageToken(pageId, userAccessToken);
  return await (
    await downloadURL(
      `https://graph.facebook.com/me?fields=id,name,picture,events,link,about&access_token=${pageAccessToken["access_token"]}`,
    )
  ).json();
}

export async function getPageToken(pageId: string, userAccessToken: string) {
  const pageAccessToken = (await (
    await downloadURL(
      `https://graph.facebook.com/${pageId}?fields=access_token&access_token=${userAccessToken}`,
    )
  ).json()) as { access_token: string };
  console.debug("Got page access token", pageAccessToken);
  return pageAccessToken;
}

export async function getUserProfile(
  userAccessToken: string,
): Promise<{ id: string; name: string; email: string }> {
  return (await (
    await downloadURL(
      `https://graph.facebook.com/me?fields=id,name,email,picture&access_token=${userAccessToken}`,
    )
  ).json()) as { id: string; name: string; email: string };
}

export async function producePageInformation(pageId: string) {
  // Get page token from DB
  // Eventually fallback to get user token and recall getPageToken
  // call pageInformation
}
