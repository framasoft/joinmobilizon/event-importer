import { EventAttendanceMode, IEvent } from "../../models/event.js";
import { EventStatus } from "../mobilizon.js";

export const convertStatus = (status: string) => {
  switch (status) {
    case "http://schema.org/EventCancelled":
      return EventStatus.CANCELLED;
    case "http://schema.org/EventScheduled":
    default:
      return EventStatus.CONFIRMED;
  }
};

export const eventOptions = (event: IEvent) => {
  return {
    isOnline: event.eventAttendanceMode === EventAttendanceMode.ONLINE,
  };
};
