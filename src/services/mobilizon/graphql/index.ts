import { GraphQLError } from "graphql";
import { GraphQLClient } from "graphql-request";
import type { Variables } from "graphql-request";
import { ClientError } from "graphql-request";
import { MobilizonOAuthApplicationToken } from "../../../models/mobilizon_oauth_application.model.js";
import { REFRESH_TOKEN } from "./auth.js";
import { httpProtocol } from "../../../router.js";

export declare class AbsintheGraphQLError extends GraphQLError {
  field?: string;
  code?: string;
  status_code?: number;
  details?: string;
}
export declare type AbsintheGraphQLErrors = ReadonlyArray<AbsintheGraphQLError>;

export async function graphQLQuery<T, V = any>(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
  query: string,
  variables: V | undefined = undefined,
): Promise<T> {
  const endpoint = `${httpProtocol()}://${host}/api`;
  const client = new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${authToken.accessToken}`,
    },
  });

  try {
    return await client.request<T>(query, variables as Variables);
  } catch (err) {
    if (
      err instanceof ClientError &&
      err.response.status === 401 &&
      isTokenExpired(
        JSON.parse(err.response.error as string) as AbsintheGraphQLError,
      )
    ) {
      const newAuthToken = await refreshToken(endpoint, authToken);
      client.setHeader("authorization", `Bearer ${newAuthToken.accessToken}`);
      return await client.request<T>(query, variables as Variables);
    }
    throw err;
  }
}

const refreshToken = async (
  endpoint: string,
  authToken: MobilizonOAuthApplicationToken,
): Promise<MobilizonOAuthApplicationToken> => {
  const client = new GraphQLClient(endpoint);
  const result = await client.request<{
    refreshToken: {
      accessToken: string;
      refreshToken: string;
    };
  }>(REFRESH_TOKEN, { refreshToken: authToken.refreshToken });
  authToken.accessToken = result.refreshToken.accessToken;
  authToken.refreshToken = result.refreshToken.refreshToken;
  return await authToken.save();
};

const isTokenExpired = (error: AbsintheGraphQLError) => {
  return (
    error.message === "invalid_token" && error.details === ":token_expired"
  );
};
