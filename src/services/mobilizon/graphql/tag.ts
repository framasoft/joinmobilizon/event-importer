import { gql } from "graphql-request";

export const TAG_FRAGMENT = gql`
  fragment TagFragment on Tag {
    id
    slug
    title
  }
`;
