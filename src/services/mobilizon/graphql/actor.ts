import { gql } from "graphql-request";

export const ACTOR_FRAGMENT = gql`
  fragment ActorFragment on Actor {
    id
    avatar {
      id
      url
      __typename
    }
    type
    preferredUsername
    name
    domain
    summary
    url
    __typename
  }
`;
