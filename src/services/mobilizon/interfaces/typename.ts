export type TypeNamed<T, V = string> = T & {
  __typename: V;
};
