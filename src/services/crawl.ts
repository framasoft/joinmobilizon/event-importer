import { EventBrite } from "../importers/eventbrite.js";
import { Facebook } from "../importers/facebook.js";
import { Meetup } from "../importers/meetup.js";
import { SOURCE_TYPES } from "../models/sync.js";
import { Schema } from "../importers/schema.js";
import { CrawlResult } from "../models/crawl_result.js";
import { IEvent } from "../models/event.js";
import { producePageInformation } from "./facebook.js";
import { urlContentType } from "../utils/http.js";
import { ICalendar } from "../importers/icalendar.js";

export async function crawlURL(
  url: string,
  options: { facebookToken?: string } = {},
): Promise<CrawlResult> {
  let event: IEvent | null = null;
  let title: string | undefined;
  const facebookIds = url.match(
    /(?:(?:http|https):\/\/)?(?:www.)?facebook.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[?\w\-]*\/)?(?:profile.php\?id=(?=\d.*))?([\w\-]*)?/,
  );
  if (
    url.match(/^https:\/\/www.meetup.com\/(fr-FR\/)?([\w-]+)\/events\/(\d+)/)
  ) {
    const meetup = new Meetup();
    event = (await meetup.importEvent(url)).result;
    title = event?.name;
  } else if (url.match(/^https:\/\/www.meetup.com\/(fr-FR\/)?([\w-]+)\/?/)) {
    const meetup = new Meetup();
    const result = (await meetup.importEvents(url)).result;
    if (result) {
      return {
        organizer: result,
        title: "Events for page " + result.name,
        originUrl: url,
        sourceType: SOURCE_TYPES.MEETUP_SOURCE,
      };
    }
  } else if (url.match(/^https:\/\/www\.eventbrite\.(\w\.?)+\/e\//)) {
    const eventBrite = new EventBrite();
    event = (await eventBrite.importEvent(url)).result;
    title = event?.name;
  } else if (url.match(/^https:\/\/www\.eventbrite\.(\w\.?)+\/o\//)) {
    const eventBrite = new EventBrite();
    const result = (await eventBrite.importEvents(url)).result;
    if (result) {
      return {
        organizer: result,
        title: "Events for page " + result.name,
        originUrl: url,
        sourceType: SOURCE_TYPES.EVENTBRITE_SOURCE,
      };
    }
  } else if (url.startsWith("https://www.facebook.com/events/")) {
    const fb = new Facebook();
    event = (await fb.importEvent(url)).result;
    title = event?.name;
  } else if (facebookIds?.[1]) {
    // const page = await producePageInformation(facebookIds?.[1]);
    // // @ts-ignore
    // return {
    //   organizer: page,
    //   title: "Events for Facebook page",
    //   originUrl: url,
    //   sourceType: SOURCE_TYPES.FB_SOURCE,
    // };
  } else if (await isUrlICS(url)) {
    const ics = new ICalendar();
    await ics.downloadData(url);
    if (ics.countSubComponents() > 1) {
      const result = (await ics.importEvents(url)).result;
      if (result) {
        return {
          organizer: result,
          title: "Events for iCalendar source",
          originUrl: url,
          sourceType: SOURCE_TYPES.ICS_SOURCE,
        };
      }
    }
    event = (await ics.importEvent(url)).result;
    title = event?.name;
  } else {
    const schema = new Schema();
    event = (await schema.importEvent(url)).result;
    title = event?.name;
  }
  return { event, title, originUrl: url };
}

const isUrlICS = async (url: string) => {
  const contentType = await urlContentType(url);
  return (
    contentType != null &&
    (contentType.startsWith("text/calendar") ||
      contentType.startsWith("application/octet-stream"))
  );
};
