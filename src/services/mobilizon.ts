import fetch from "node-fetch";
import { graphQLQuery } from "./mobilizon/graphql/index.js";
import {
  LOGGED_USER_INFO,
  LOGGED_USER_PROFILES,
} from "./mobilizon/graphql/user.js";
import {
  CREATE_EVENT,
  INTERACT,
  UPDATE_EVENT,
  memberships,
} from "./mobilizon/graphql/events.js";
import { ILoggedUser } from "./mobilizon/interfaces/loggedUser.js";
import { IEvent } from "../models/event.js";
import { TypeNamed } from "./mobilizon/interfaces/typename.js";
import { MobilizonOAuthApplicationToken } from "../models/mobilizon_oauth_application.model.js";
import { httpProtocol } from "../router.js";

export interface Application {
  client_id: string;
  client_secret: string;
  name: string;
  redirect_uri: string[];
  scope: string;
  website: string;
}

export interface Error {
  error: "invalid_scope" | "server_error" | "slow_down";
  error_description: string;
}

export async function registerApplication(
  host: string,
  name: string,
  redirect_uris: string[],
  scopes: string,
  website: string | undefined = undefined,
): Promise<Application | undefined> {
  const data = new URLSearchParams();
  data.append("name", name);
  redirect_uris.forEach((redirect_uri) => {
    data.append("redirect_uri", redirect_uri);
  });
  data.append("scope", scopes);
  if (website) {
    data.append("website", website);
  }
  console.debug("data", data.toString());

  try {
    const res = await fetch(`${httpProtocol()}://${host}/apps`, {
      method: "POST",
      body: data,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        // 'Accept': 'application/json'
      },
    });
    const result = (await res.json()) as Application | Error;
    if (result.hasOwnProperty("error")) {
      throw result;
    }
    return result as Application;
  } catch (error) {
    console.error(error);
  }
}

export async function userInfo(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
) {
  return (
    await graphQLQuery<{
      loggedUser: ILoggedUser;
    }>(host, authToken, LOGGED_USER_INFO)
  ).loggedUser;
}

export async function userProfiles(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
) {
  return (
    await graphQLQuery<{
      loggedUser: Pick<ILoggedUser, "id" | "actors">;
    }>(host, authToken, LOGGED_USER_PROFILES)
  ).loggedUser;
}

export async function userMemberships(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
) {
  return (
    await graphQLQuery<{
      loggedUser: Pick<ILoggedUser, "id" | "memberships">;
    }>(host, authToken, memberships)
  ).loggedUser.memberships;
}

interface AddressInput {
  geom: string;
  street?: string | null;
  locality?: string | null;
  postalCode?: string | null;
  region?: string | null;
  country?: string | null;
  description: string;
  type?: string | null;
  url?: string;
  id?: string;
  originId?: string;
}

export enum EventStatus {
  TENTATIVE = "TENTATIVE",
  CONFIRMED = "CONFIRMED",
  CANCELLED = "CANCELLED",
}

export interface EventOptionsInput {
  maximumAttendeeCapacity?: number;
  remainingAttendeeCapacity?: number;
  timezone?: string;
  isOnline?: boolean;
}

export interface EventVariables {
  organizerActorId: string;
  attributedToId?: string;
  title: string;
  description: string;
  beginsOn: Date;
  endsOn?: Date;
  status?: EventStatus;
  // visibility?: EventVisibility
  // joinOptions?: EventJoinOptions
  externalParticipationUrl?: string;
  draft?: boolean;
  tags?: string[];
  // picture?: MediaInput
  onlineAddress?: string;
  phoneAddress?: string;
  // category?: EventCategory
  physicalAddress?: AddressInput;
  options?: EventOptionsInput;
  // contacts?: Contact[]
  // metadata?: EventMetadataInput[]
}

export async function createEvent(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
  eventVariables: EventVariables,
) {
  return await graphQLQuery<
    {
      createEvent: IEvent;
    },
    EventVariables
  >(host, authToken, CREATE_EVENT, eventVariables);
}

export async function updateEvent(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
  eventVariables: EventVariables & { id: string },
) {
  return await graphQLQuery<{
    updateEvent: IEvent;
  }>(host, authToken, UPDATE_EVENT, eventVariables);
}

export async function searchEventByUrl(
  host: string,
  authToken: MobilizonOAuthApplicationToken,
  eventUrl: string,
) {
  return await graphQLQuery<{
    interact: TypeNamed<{ id: string }, "Group" | "Event">;
  }>(host, authToken, INTERACT, { uri: eventUrl });
}
