const encryptionKey = process.env.ENCRYPTION_KEY;
const secret = process.env.SECRET;

if (!encryptionKey) {
  throw "Encryption key is missing. You need to provide ENCRYPTION_KEY to the environment.";
}

if (!secret) {
  throw "Secret is missing. You need to provide SECRET to the environment.";
}

export const config = {
  PORT: process.env.PORT || 3000,
  WEB_PORT: process.env.WEB_PORT || 443,
  ENCRYPTION_KEY: encryptionKey,
  SECRET: secret,
  REFRESHMENT_PERIOD_HOURS: Math.max(
    Number(process.env.REFRESHMENT_PERIOD_HOURS) || 2,
    1,
  ),
};
