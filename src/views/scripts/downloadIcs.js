const downloadICS = (e) => {
  console.debug("Called downloadIcs with", e);
  const data = e.target.dataset.icsText;
  const filename = e.target.dataset.icsName;
  download(filename, data);
};

const download = (filename, text) => {
  console.debug("Called download with", filename, text);
  const element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/calendar;charset=utf-8," + encodeURIComponent(text),
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
};

document.addEventListener("DOMContentLoaded", (event) => {
  const buttons = document.querySelectorAll('button[data-ics-download="true"]');

  buttons.forEach((button) => {
    button.addEventListener("click", (event) => {
      downloadICS(event);
    });
  });
});

// window.downloadICS = downloadICS;
