FROM node:20-alpine

COPY . .

RUN npm install
RUN npm install sqlite3
RUN npm run build   # node_modules/typescript/bin/tsc
RUN npm exec -- tailwindcss -i src/input.css -c tailwind.config.js -o dist/output.css
RUN cp -r public/* dist/

EXPOSE 3000

CMD ["docker/entrypoint.sh"]
